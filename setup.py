import setuptools

# with open("README.md", "r") as fh:
#     long_description = fh.read()


setuptools.setup(
    name="llphd-any2any",
    version="0.0.1",
    author="Lucy Linder",
    author_email="lucy.derlin@gmail.com",
    description="Generic seq2seq with attention (written for SAMPA)",
    license='Apache License 2.0',
    long_description="TODO",
    url="TODO",

    packages=setuptools.find_packages(),
    # package_data={'': ['*.yml']},  # include yaml and pickle from any module
    entry_points={
        'console_scripts': [
            'any2any = any2any.__main__:main',
        ]
    },
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: Apache 2.0 License",
        "Operating System :: OS Independent",
    ),
    install_requires=[
        'torch',  # ==1.0.1.post2
        'torchvision',  # ==0.2.2.post3
        'pandas',  # ==0.24.2
        'jiwer',  # ==1.3.2'
        'click'  # ==6.7
    ]
)
