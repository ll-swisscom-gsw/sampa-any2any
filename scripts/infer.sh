#!/usr/bin/env bash

INPUT=${1:-"${EXPNAME}_inputs.txt"}
shift 

if [ -z "$INPUT" ]; then
	echo missing input file
	exit 1
fi

REAL_INPUT=$(realpath $INPUT)
cd $OUT_DIR

any2any infer \
    --vocabs $OUT_DIR/vocabs.pth \
    --model $OUT_DIR/best_model.chk \
    --input_file $INPUT $@

