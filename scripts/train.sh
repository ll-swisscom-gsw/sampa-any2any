#!/usr/bin/env bash

#set -x

mkdir -p $OUT_DIR
cd $OUT_DIR

(time any2any train \
    --joiners "$J1" "$J2" \
    --train_file $TRAIN_FILE \
    --test_file $TEST_FILE \
    --dec_embedding $DEC_EMBEDDING \
    --enc_embedding $ENC_EMBEDDING \
    --test $@ ) 2>&1 | tee train.log

