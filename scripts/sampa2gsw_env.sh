#!/usr/bin/env bash

expname=sampa2gsw
export BASE_DIR=/home/llinder/git/sampa-seq2seq

# experiment params
export ENC_EMBEDDING=128
export DEC_EMBEDDING=64

# data params
data_dir=$BASE_DIR/data
export TRAIN_FILE=$data_dir/train_pairs.gsw.csv
export TEST_FILE=$data_dir/test_pairs.gsw.csv
export J1=" "
export J2=""

# output params
export OUT_DIR=$BASE_DIR/experiments/out/$expname

# python setup
export PYTHONUNBUFFERED=1
