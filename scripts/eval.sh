#!/usr/bin/env bash

cd $OUT_DIR

any2any eval \
    --vocabs vocabs.pth \
    --model best_model.chk \
    --test_file $TEST_FILE $@ 2>&1 | tee eval.log

