import click
import pandas as pd
from jiwer import wer


def load_csv(file):
    return pd.read_csv(file).text.values


def load_file(file):
    with open(file, encoding='utf-8') as f:
        return [s.replace('<eos>', '').strip() for s in f if s]


def compute_wer_words(refs, hyps):
    return pd.DataFrame([[ref, hyp, wer(list(ref), list(hyp)) if hyp else len(ref)] for ref, hyp in zip(refs, hyps)],
                        columns=['ref', 'hyp', 'wer'])


def compute_wer_all(refs, hyps):
    return wer(refs, hyps)


if __name__ == '__main__':
    import sys

    if len(sys.argv) < 2:
        exit(1)
    print(f'reading {sys.argv[1]}')
    data = pd.read_csv(sys.argv[1])
    refs = [(s if isinstance(s, str) else '').replace('<eos>', '') for s in data.target.values]
    hyps = [(s if isinstance(s, str) else '').replace('<eos>', '') for s in data.decoded.values]

    df = compute_wer_words(refs, hyps).sort_values('wer', ascending=False)
    wer_c = compute_wer_all(refs, hyps)
    print(f'WER word-by-word')
    print(df[['wer']].describe())
    print(f'\nWER corpus = {wer_c}')
    print('-----')
    print(f'Number of exact matches: {len(df[df.wer == 0])}')
    print('Worst samples:')
    print(df.head(20))
