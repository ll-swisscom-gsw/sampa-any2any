# -*- coding: utf-8 -*-

import csv
from functools import partial

import click
import jiwer

import pandas as pd

import torch
from torch import optim
from torch.utils.data import DataLoader

from any2any.dts import FixedDataset, Vocab, load_pairs
from any2any.luong import reload_from, DEVICE, Translater, BatchGenerator, train

click.option = partial(click.option, show_default=True)  # show default in help (see click issues #646)
cli = click.Group(context_settings=dict(help_option_names=['-h', '--help']))


def reload(model_file, vocabs_file):
    vocabs = [Vocab(*params) for params in torch.load(vocabs_file)]
    print(f'Reloaded dictionaries: {vocabs}')

    translater, epoch = reload_from(model_file, DEVICE)
    print(f'Reloaded model from epoch {epoch}')

    return translater, vocabs


def do_eval(translater, vocabs, test_set, output_file, beam_size, beam_return):
    data = []
    headers = ['source', 'target', 'wer', 'decoded']

    with open(output_file, 'w', encoding='utf-8') as f:
        csv_writer = csv.writer(f)
        translater.eval()

        with torch.no_grad():
            if beam_size > 1:
                headers += [f'decoded{i}' for i in range(1, beam_return)]
                if beam_return > 1:
                    headers += [f'recall']
                csv_writer.writerow(headers)
                for s, t in test_set:
                    seqs = translater.generate_beam(s, beam_size, beam_return)

                    source = vocabs[0].from_indexes(s)
                    target = vocabs[1].from_indexes(t)
                    decoded = [
                        vocabs[1].from_indexes(seq.output)
                        for seq in seqs
                    ]
                    wer = jiwer.wer(vocabs[1].split(target), vocabs[1].split(decoded[0]))
                    row = [source, target, wer] + decoded
                    if beam_return > 1: row += [int(target in decoded)]
                    csv_writer.writerow(row)
                    data.append(row)

            else:
                csv_writer.writerow(headers)

                batch_generator = BatchGenerator(test_set, batch_size=30)
                for sources, targets in batch_generator:
                    ys = translater.generate_greedy(sources)

                    for s, t, y in zip(sources, targets, ys):
                        source = vocabs[0].from_indexes(s)
                        target = vocabs[1].from_indexes(t)
                        decoded = vocabs[1].from_indexes(y)
                        wer = jiwer.wer(vocabs[1].split(target), vocabs[1].split(decoded))

                        row = [source, target, wer, decoded]
                        csv_writer.writerow(row)
                        data.append(row)

    df = pd.DataFrame(data, columns=headers)
    # ==== SCORING
    print(df[['wer']].describe())
    # print('\nWer corpus:', jiwer.wer(df.target.values, df.decoded.values)) # interesting, but soooo slow
    print(f'Number of exact matches: {len(df[df.wer == 0])}/{len(df)}')
    if beam_size > 1 and beam_return > 1:
        print(f'Recall@{beam_return}: {df.recall.sum()}/{len(df)}')
    print('\nWorst samples:')
    print(df.sort_values('wer', ascending=False).head(20))
    print('Results written to:', output_file)


@cli.command('train')
@click.option('--expname', default='', type=str, help='Experiment name.', show_default=False)
# data parameters
@click.option('--train_file', required=True, help='Train file: CSV with 2 columns (src,tgt).')
@click.option('--test_file', required=True, help='Test file: CSV with 2 columns (src,tgt).')
@click.option('--joiners', type=str, nargs=2, default=(' ', ' '),
              help='Characters to split (src,tgt) into tokens. [default: space]', show_default=False)
# model parameters
@click.option('--enc_embedding', type=int, default=128, help='Encoder embedding dim.')
@click.option('--dec_embedding', type=int, default=128, help='Decoder embedding dim.')
@click.option('--hidden_size', type=int, default=512, help='Hidden size.')
@click.option('--num_layers', type=int, default=2, help='LSTM layers in encoder/decoder.')
@click.option('--dropout', type=float, default=0.2, help='Dropout after embedding in encoder/decoder.')
@click.option('--max_length', type=int, default=20, help='Max length of generated output.')
# train parameters
@click.option('--lr', type=float, default=0.01, help='Learning rate (SGD).')
@click.option('--lr_step', type=int, default=10, help='Decrease LR every X epochs.')
@click.option('--lr_gamma', type=float, default=0.1, help='Decrease LR by G every step.')
@click.option('--epochs', type=int, default=30, help='Number of epochs.')
@click.option('--batch_size', type=int, default=8, help='Batch size.')
@click.option('--teacher_ratio', type=float, default=0.75, help='Teacher forcing ratio. 1=always, 0=never.')
@click.option('--chk_every', default=-1, type=int, help='Create a checkpoint every X epochs.')
@click.option('--stop', type=int, default=5, help='Stop if no improvements after X epochs.')
# eval parameters
@click.option('--test/--no-test', default=False, help='Eval the best model.')
@click.option('--beam_size', type=int, default=1, help='Beam width. 1=greedy.')
@click.option('--beam_return', type=int, default=1, help='Top k hypotheses to keep.')
def cli_train(expname,
              train_file, test_file, joiners,
              enc_embedding, dec_embedding, hidden_size, num_layers, dropout, max_length,
              lr, lr_step, lr_gamma, epochs, batch_size, teacher_ratio, chk_every, stop,
              test, beam_size, beam_return):
    """
    Train a seq2seq model.

    The best model will be saved in `<expname>best_model.chk`.
    The vocabulary is extracted from the train and test files and saved in `<expname>vocabs.pth`.
    Checkpoints (depending on :chk_every:) are saved in `<expname>model-<epoch>.chk`.

    The :joiners: tell the program how to split the sources and targets into tokens.
    Use ' ' to break word by word, or '' to break char by char.
    """
    if chk_every < 0: chk_every = epochs + 1  # no checkpoints
    print('Using arguments:', locals())

    dts = FixedDataset(train_file=train_file, test_file=test_file, joiners=joiners, device=DEVICE)
    print(f'Data loaded. {dts}')
    torch.save([
        (dts.vocabs[0].joiner, dts.vocabs[0].s2i),
        (dts.vocabs[1].joiner, dts.vocabs[1].s2i)
    ], f'{expname}vocabs.pth')
    print(f'Vocabulary written to {expname}vocabs.pth')

    translater_params = dict(
        max_translation_length=max_length,
        vocab_sizes=[len(v) for v in dts.vocabs],
        hidden_size=hidden_size,
        encoder_kwargs=dict(embedding_dim=enc_embedding, num_layers=num_layers, dropout=dropout),
        decoder_kwargs=dict(embedding_dim=dec_embedding, num_layers=num_layers, dropout=dropout)
    )

    translater = Translater(**translater_params, device=DEVICE).to(DEVICE)
    optimizer = optim.SGD(translater.parameters(), lr=lr)
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=lr_step, gamma=lr_gamma)

    print('\n==== MODEL SUMMARY')
    print(translater)

    print('\n==== TRAIN')
    best_model_state, epoch = train(
        translater, optimizer, scheduler, dts.train,
        batch_size, epochs, stop, teacher_ratio, chk_every, expname)

    if test:
        print(f'\n==== EVAL on best model (epoch={epoch})')
        translater.load_state_dict(best_model_state)
        do_eval(translater, dts.vocabs, dts.test, f'{expname}eval_{beam_size}-{beam_return}.csv', beam_size,
                beam_return)


@cli.command('eval')
# model parameters
@click.option('--model', required=True, help='Path to model.chk')
@click.option('--vocabs', required=True, help='Path to vocabs.pth')
# data parameters
@click.option('--test_file', type=str, required=True, help='Test file. CSV of two columns: source, target.')
@click.option('--output_file', type=str, help='Output file.')
# eval parameters
@click.option('--beam_size', type=int, default=1, help='Beam width. 1=greedy.')
@click.option('--beam_return', type=int, default=1, help='Number of return if beam is used.')
def cli_eval(model, vocabs, test_file, output_file, beam_size, beam_return):
    """
    Evaluate a model using WER.

    If beam decoding is used, the WER is computed on the first decoded output.
    The input file is a csv with columns `source, target`.
    The output is a CSV file with the headers: `source, decoded [, decodedX]`
    with X a number from 1 to beam_return - 1.
    """
    if output_file is None:
        output_file = f'eval_{beam_size}-{beam_return}.csv'
    print('Using arguments:', locals())
    translater, vocabs = reload(model, vocabs)

    with open(test_file, encoding='utf-8') as f:
        test_set = [[torch.LongTensor(v.to_indexes(s.strip())).to(DEVICE) for v, s in zip(vocabs, pair)]
                    for pair in load_pairs(test_file)]

    do_eval(translater, vocabs, test_set, output_file, beam_size, beam_return)


@cli.command('infer')
# model parameters
@click.option('--model', required=True, help='Path to model.chk')
@click.option('--vocabs', required=True, help='Path to vocabs.pth')
# data parameters
@click.option('--input_file', required=True, help='Input file, one sample per line.')
@click.option('--output_file', help='Output file.')
# infer parameters
@click.option('--beam_size', type=int, default=4, help='Beam width. 1=greedy.')
@click.option('--beam_return', type=int, default=5, help='Number of results per sample.')
def cli_infer(model, vocabs, input_file, output_file, beam_size, beam_return):
    """
    Run inference using beam or greedy decoding.

    The input file must be one input per line. The output is a CSV file with the headers:
    `source, decoded [, decodedX]` with X a number from 1 to beam_return - 1.
    """
    if output_file is None:
        output_file = f'inferred_{beam_size}-{beam_return}.csv'

    print('Using arguments:', locals())
    translater, vocabs = reload(model, vocabs)

    with open(input_file, encoding='utf-8') as f:
        samples = [torch.LongTensor(vocabs[0].to_indexes(s.strip())).to(DEVICE)
                   for s in f]

    headers = ['source', 'decoded']

    with open(output_file, 'w', encoding='utf-8') as f:
        csv_writer = csv.writer(f)

        with torch.no_grad():
            if beam_size > 1:
                headers += [f'decoded{i}' for i in range(1, beam_size)]
                csv_writer.writerow(headers)
                for s in samples:
                    seqs = translater.generate_beam(s, beam_size, beam_return)
                    source = vocabs[0].from_indexes(s)
                    decoded = [
                        vocabs[1].from_indexes(seq.output)
                        for seq in seqs
                    ]
                    csv_writer.writerow([source] + decoded)

            else:
                csv_writer.writerow(headers)

                batch_generator = DataLoader(samples, batch_size=30, shuffle=False)
                for sources in batch_generator:
                    ys = translater.generate_greedy(sources)

                    for s, y in zip(sources, ys):
                        source = vocabs[0].from_indexes(s)
                        decoded = vocabs[1].from_indexes(y)
                        csv_writer.writerow([source, decoded])

    print(f'Results written to {output_file}.')
