import torch
from collections import defaultdict
import pandas as pd
from io import open


def load_pairs(file):
    return pd.read_csv(file).values


def load_vocab(file):
    with open(file, encoding='utf-8') as f:
        lines = [s.strip() for s in f if s and not s.isspace()]
        return {i: s for i, s in enumerate(lines)}


class Tokens:
    PAD = 0
    SOS = 1
    EOS = 2


class Vocab:

    def __init__(self, joiner='', s2i=None):
        self.s2i = defaultdict(lambda: len(self.s2i))
        self.i2s = None
        self.joiner = joiner

        if s2i is not None:
            self.s2i = s2i
            self.freeze()
        else:
            for special in ['', '<SOS>', '<EOS>']:
                _ = self.s2i[special]

    def to_indexes(self, s, add_eos=True):
        idxs = [self.s2i[c] for c in self.split(s)]
        return idxs + [Tokens.EOS] if add_eos else idxs

    def from_indexes(self, idxs, strip=True):
        if isinstance(idxs, torch.Tensor): idxs = idxs.tolist()
        if strip:
            if idxs[0] == Tokens.SOS:
                idxs = idxs[1:]
            if Tokens.EOS in idxs:
                idxs = idxs[:idxs.index(Tokens.EOS)]
        return self.joiner.join([self.i2s[i] for i in idxs])

    def split(self, s):
        return list(s) if self.joiner == '' else s.split(self.joiner)

    def freeze(self):
        self.s2i = dict(self.s2i)
        self.i2s = {v: k for k, v in self.s2i.items()}

    def __len__(self):
        return len(self.s2i)

    def __repr__(self):
        return f'[len={len(self)-3}, {self.i2s[3] if len(self) >= 3 else ""}]'


class FixedDataset:

    def __init__(self, train_file, test_file, device, vocabs=None, joiners=('', '')):
        train_pairs = pd.read_csv(train_file).values
        test_pairs = pd.read_csv(test_file).values

        create_vocab = vocabs is None

        if create_vocab:
            self.vocabs = [Vocab(j) for j in joiners]
        else:
            self.vocabs = [Vocab(j, s2i) for (j, s2i) in zip(joiners, vocabs)]

        self.train = self.encode(train_pairs, device)
        self.test = self.encode(test_pairs, device)

        if create_vocab:
            for v in self.vocabs: v.freeze()

    def encode(self, pairs, device):
        return [[
            torch.LongTensor(self.vocabs[i].to_indexes(s)).to(device)
            for i, s in enumerate(pair)
        ] for pair in pairs]

    def __repr__(self):
        return f'train={len(self.train)},test={len(self.test)},v1={self.vocabs[0]},v2={self.vocabs[1]}'
