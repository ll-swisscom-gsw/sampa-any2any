# -*- coding: utf-8 -*-


import time
import types

import torch
import torch.nn.functional as F
from torch import nn
from torch.utils.data import DataLoader

"""
# make all deterministic
seed = 0
np.random.seed(seed)
torch.manual_seed(seed)
torch.cuda.manual_seed_all(seed)
random.seed(seed)
"""

# use GPU is available
DEVICE = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class Tokens:
    PAD = 0
    SOS = 1
    EOS = 2


class BatchGenerator(DataLoader):
    def __init__(self, pairs, batch_size, shuffle=True):
        super().__init__(
            pairs, batch_size=batch_size, shuffle=shuffle,
            collate_fn=lambda batch: zip(*batch))
        # collate_fn=lambda batch: zip(*sorted(batch, key=lambda p: len(p[0]), reverse=True)))


"""# Models

## Encoder
"""


class SimpleEncoder(nn.Module):

    def __init__(self, source_vocab_size, hidden_size, embedding_dim=512, num_layers=1, bidirectional=True, dropout=.0):
        super().__init__()

        self.input_size = source_vocab_size
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.bidirectional = bidirectional

        self.embedding = nn.Embedding(
            num_embeddings=source_vocab_size,
            embedding_dim=embedding_dim)

        self.dropout = nn.Dropout(dropout) if dropout > .0 else None

        self.gru = nn.GRU(
            input_size=embedding_dim,
            hidden_size=hidden_size,
            num_layers=num_layers,
            bidirectional=bidirectional,
            batch_first=True)

        if bidirectional:
            self.merge_dirs = nn.Linear(2 * hidden_size, hidden_size)

    def sort_sequences(self, inputs, lengths):
        # see https://city.shaform.com/en/2019/01/15/sort-sequences-in-pytorch/
        lengths_sorted, sorted_idx = lengths.sort(descending=True)
        _, unsorted_idx = sorted_idx.sort()
        return inputs[sorted_idx], lengths_sorted, unsorted_idx

    def forward(self, input_seqs, input_lengths):
        """
        :param input_seqs: tensor of shape [batch_size x seq_length], no need to be sorted
        :param input_lengths: tensor of shape [batch_size] with the actual length of each sequence (without paddding)
        :return: a tuple:
             - hiddens of shape [batch_size x seq_length x hidden_size] (used for attention)
             - last_hidden of shape [num_layers x batch_size x hidden_size] (used to init the decoder)
        """
        # sort sequences by longest first (required by rnn packed sequences)
        inputs, sorted_lengths, unsorted_idx = self.sort_sequences(input_seqs, input_lengths)

        embedded = self.embedding(inputs)
        if self.dropout is not None:
            embedded = self.dropout(embedded)
        # embedded of size: [batch_size x seq_length x embedding_dim]

        packed = nn.utils.rnn.pack_padded_sequence(embedded, sorted_lengths, batch_first=True)
        # packed is necessary to call an RNN with a sequence

        hiddens_packed, last_hiddens = self.gru(packed)  # the initial state is set to zero by default
        hiddens, _ = nn.utils.rnn.pad_packed_sequence(hiddens_packed, batch_first=True)
        # "unsort" the output to match the batch initial order
        hiddens, last_hidden = hiddens.index_select(0, unsorted_idx), last_hiddens.index_select(1, unsorted_idx)

        # hiddens contain the hidden state of the last layer at each time t
        #   * size of [batch_size x seq_length x (num_directions * hidden_size)]
        #   * !! padded, so for the last batch, we will have a lot of hidden states to 0
        # last_hidden contains the latest NON-ZERO hidden state of ALL layers
        #   * size of [(num_layers * num_directions) x batch_size x hidden_size]
        #   * !! doesn't match hiddens[-1,:,:] if the batches don't have the same size
        #   * left2right = last_hidden[0], right2left = last_hidden[1] if bidirectional

        if self.gru.bidirectional:
            # merge the hidden states in both directions
            hiddens = torch.tanh(self.merge_dirs(
                torch.cat((hiddens[:, :, :self.hidden_size], hiddens[:, :, self.hidden_size:]), dim=-1)
            ))
            # hiddens now has a size of [batch_size x seq_length x hidden_size]
        return hiddens, last_hidden


"""## AttentionDecoder"""


class Attention(nn.Module):

    def __init__(self, hidden_size):
        super().__init__()
        self.hidden_size = hidden_size
        self.Wa = nn.Linear(in_features=hidden_size, out_features=hidden_size)

    def forward(self, encoder_outputs, current_hidden, mask=None):
        """
        General attention as defined by Luong et al. (note: here, seq_length is the length of the input seqs)
        :param encoder_outputs: encoder outputs at each time step, of shape [batch_size x seq_length x hidden_size]
        :param current_hidden: output of the decoder rnn at this time step, of shape [batch_size x 1 x hidden_size]
        :param mask: input mask of shape [batch_size x input_seq_length], where 1 means padding
        :return: the attention weights of shape [batch_size x 1 x seq_len]
        """
        # linear: xA.t() + b, (N, *, in\_features) -> (N,∗,out_features)
        # basically encoder_inputs * Wa.t() + b
        WaHs = self.Wa(encoder_outputs)
        # WaHs of shape [batch_size x seq_len x hidden_size]

        # multiplication element-wise of the two tensors, then sum along the first two dimensions
        # -> we multiply the encoder hidden state at each time step (hs_i) with the last hidden state (ht)
        dot_product = torch.sum(current_hidden * WaHs, dim=2)
        # current_hidden * WaHs of shape [batch_size x seq_len x hidden_size]
        # dot_product of shape [batch_size x seq_len]

        if mask is not None:
            # ensure the attention scores are zero for padded inputs
            # note: -inf because afterwards we apply softmax, and e^{-inf} = 1/e^{inf} = 0
            dot_product.masked_fill_(mask, -float('inf'))

        # here, we want the weights for one batch to equal 1
        ai = F.softmax(dot_product, dim=1)
        # ait of shape [batch_size x seq_length]
        assert abs(ai[0].sum().item() - 1) < 1E-5  # ensure batches sum to 1

        return ai.unsqueeze(1)
        # return weights in the shape [batch_size x 1 x seq_length]


class AttentionDecoder(nn.Module):

    def __init__(self, target_vocab_size, hidden_size, embedding_dim=512, num_layers=1, dropout=.0):
        super().__init__()

        self.inout_size = target_vocab_size
        self.hidden_size = hidden_size
        self.num_layers = num_layers

        self.embedding = nn.Embedding(
            num_embeddings=target_vocab_size,
            embedding_dim=embedding_dim)

        self.dropout = nn.Dropout(dropout) if dropout > .0 else None

        self.attn = Attention(
            hidden_size=hidden_size)

        self.gru = nn.GRU(
            input_size=embedding_dim,
            hidden_size=hidden_size,
            num_layers=num_layers,
            batch_first=True)

        self.combine = nn.Linear(
            in_features=2 * hidden_size,
            out_features=hidden_size
        )
        self.out = nn.Linear(
            in_features=hidden_size,
            out_features=target_vocab_size)

    def forward(self, last_y, last_hidden, encoder_outputs, src_masks=None):
        """
        Does one step of decoding using Luong's general attention mechanism.
        :param last_y: output of the previous step of shape [batch_size x 1]
        :param last_hidden: last hidden state of shape [num_layers x batch_size x hidden_size]
        :param encoder_outputs: the outputs of the encoder at each time step of shape [batch_size x seq_length x hidden_size]
        :param src_masks: boolean matrix of shape [batch_size x seq_length], where 1 means padding
        :return: predictions of shape [batch_size x vocab_size] and
                 hidden state of shape [num_layers x batch_size x hidden_size]
        """
        # last_y of shape (1, batch, 1)
        # last_hidden of shape (num_layers, batch, hidden_size)
        # encoder_outputs of shape (seq_len, batch, hidden_size)

        embedded = self.embedding(last_y)
        if self.dropout is not None:
            embedded = self.dropout(embedded)
        # embedded is of shape [batch_size x seq_length x embedding_dim]

        rnn_output, hidden = self.gru(embedded, last_hidden)
        # rnn_output of shape [batch_size x 1 x hidden_size]
        # hidden of shape [num_layers x batch_size x hidden_size]

        attn_weights = self.attn(encoder_outputs, rnn_output, src_masks)
        # attn_weights of shape [batch_size x 1 x seq_len]

        # do the weighted average over all the source hidden states
        #   bmm => (b x n x m) @ (b x m x p) = (b x n x p)
        # attn_weights @ encoder_outputs is thus:
        #   [batch_size x 1 x seq_len] @ [batch_size x seq_length x hidden_size] =
        #   [batch_size x 1 x hidden_size]
        context = torch.bmm(attn_weights, encoder_outputs)
        # context of shape [batch_size x 1 x hidden_size]

        # remove the dimensions with shape 1, ending with
        # matrices of shape [batch_size x hidden_size] easily concatenated
        concat = torch.cat((context.squeeze(1), rnn_output.squeeze(1)), dim=1)
        # concat of shape [batch_size x 2*hidden_size]

        # apply equation 5 of Luong et al. : h~ = tanh(Wc[ct;ht])
        h_hat = torch.tanh(self.combine(concat))
        # h_hat of shape [batch_size x hidden_size]

        y = self.out(h_hat)
        # y of shape [batch_size x vocab_size]

        return y, hidden
        # return of shapes
        #    (batch, vocab_size), (num_layers, batch, hidden_size)


"""## Translater"""


class Translater(nn.Module):

    def __init__(self, max_translation_length, vocab_sizes, device, hidden_size=256,
                 encoder_kwargs={}, decoder_kwargs={},
                 pad_token=0, sos_token=1, eos_token=2):

        super().__init__()

        self.init_params = {k: v for k, v in locals().items()
                            if k not in ['self', 'device'] and not k.startswith('__')}

        self.pad_token, self.sos_token, self.eos_token = pad_token, sos_token, eos_token
        self.max_translation_length = max_translation_length
        self.device = device

        self.encoder = SimpleEncoder(
            source_vocab_size=vocab_sizes[0],
            hidden_size=hidden_size,
            **encoder_kwargs).to(device)

        self.decoder = AttentionDecoder(
            target_vocab_size=vocab_sizes[1],
            hidden_size=hidden_size,
            **decoder_kwargs).to(device)

    def _zero_pad(self, batch):
        """
        :param batch: list of tensors
        :return: tensor of shape [batch_size x max_seq_length] and tensor of batch lengths
        """
        lengths = list(map(len, batch))
        tensors = nn.utils.rnn.pad_sequence(batch, self.pad_token).transpose(0, 1)
        return tensors.to(self.device), torch.LongTensor(lengths).to(DEVICE)

    def _encoder_step(self, batch_x):
        """
        :param batch_x: the input batch of shape [batch_size x seq_length]
        :return:
            - initial decoder inputs (SOS) of shape [batch_size x 1]
            - initial decoder state of shape [num_layers x batch_size x hidden_size]
            - encoder_outputs (for attention) of shape [batch_size x seq_length x hidden_size]
            - src_masks of shape [batch_size x seq_length]
        """
        batch_size = len(batch_x)
        # pad the sequence
        input_seqs, input_lengths = self._zero_pad(batch_x)
        # encode the source sentence
        encoder_outputs, encoder_state = self.encoder(input_seqs, input_lengths)
        # decode the translation
        # here, just keep the state of the forward pass (shape = (num_layers * num_directions, batch_size, hidden_size))
        # note that using a GPU, omitting the .contiguous call raises an exception "rnn: hx is not contiguous"
        decoder_state = encoder_state \
                            .view(self.encoder.num_layers, 2 * self.encoder.bidirectional, batch_size,
                                  self.encoder.hidden_size)[-self.decoder.num_layers:, 0, :, :] \
            .contiguous() \
            .view(self.decoder.num_layers, batch_size, self.decoder.hidden_size)

        # input must be of size: [batch_size x seq_length], hence [batch_size x 1]
        decoder_input = torch.LongTensor([[self.sos_token]] * batch_size).to(self.device)

        # masking used for the decoder's attention, 1 means padding
        src_masks = (input_seqs == self.pad_token)

        return decoder_input, decoder_state, encoder_outputs, src_masks

    def forward(self, batch_x, batch_y, loss_function=None, teacher_forcing_ratio=0.2):
        """
        :param batch_x: list of input tensors
        :param batch_y: list of target tensors
        :param loss_function: cross entropy loss object
        :param teacher_forcing_ratio: ratio of teacher forcing (i.e. feed the decoder with the target), 1=always, 0=never
        :return: the total loss over the batch
        """
        loss = 0

        # zero pad the target
        target_seqs, target_lengths = self._zero_pad(batch_y)
        max_length = target_lengths[0]

        # encode
        decoder_input, decoder_state, encoder_outputs, src_masks = self._encoder_step(batch_x)

        # decode one time step at a time
        use_teacher_training = torch.rand(1).item() < teacher_forcing_ratio
        for t in range(max_length):
            logits, decoder_state = self.decoder(decoder_input, decoder_state, encoder_outputs, src_masks)
            loss += loss_function(logits, target_seqs[:, t])

            if use_teacher_training:
                # use a teacher forcing: feed the decoder with the actual target
                decoder_input = target_seqs[:, t].unsqueeze(1)
            else:
                # feed the decoder with its own prediction
                probas = F.log_softmax(logits, dim=1)
                best_values, best_indexes = probas.data.topk(1)
                predictions = best_indexes.detach()
                decoder_input = predictions

        return loss

    def generate_greedy(self, batch_x):
        """
        :param batch_x: a list of input tensors
        :return: a list of prediction tensors
        """

        batch_size = len(batch_x)
        translations = torch.zeros(batch_size, self.max_translation_length, device=self.device, dtype=torch.long)
        decoder_input, decoder_state, encoder_outputs, src_masks = self._encoder_step(batch_x)

        for t in range(self.max_translation_length):
            decoder_output, decoder_state = self.decoder(decoder_input, decoder_state, encoder_outputs, src_masks)
            probas = F.softmax(decoder_output, dim=1)
            best_values, best_indexes = probas.data.topk(1)
            predictions = best_indexes.detach()
            decoder_input = predictions
            # get translations
            translations[:, t] = best_indexes.squeeze()
            # stop if reached the end of each batch
            if ((predictions == self.eos_token) + (predictions == self.pad_token)).sum() == batch_size:
                break

        return translations.tolist()

    def generate_beam(self, x, beam_width=4, n_return=4):

        decoder_input, decoder_state, encoder_outputs, src_masks = self._encoder_step(x.unsqueeze(0))

        def decode_step(inpts, state, k):
            repeat = len(inpts)

            out, new_state = self.decoder(
                torch.LongTensor([b[-1:] for b in inpts]).to(DEVICE),
                torch.stack(state, dim=1).to(DEVICE),
                encoder_outputs.repeat(repeat, 1, 1),
                src_masks.repeat(repeat, 1),
            )

            probas = F.log_softmax(out, dim=1)
            best_values, best_indexes = probas.data.topk(k)
            return best_indexes.tolist(), best_values, new_state.transpose(0, 1)

        from any2any.beam_search import SequenceGenerator
        sg = SequenceGenerator(
            decode_step,
            Tokens.EOS,
            beam_size=beam_width,
            return_count=n_return,
            max_sequence_length=self.max_translation_length
        )

        seqs = sg.beam_search(
            decoder_input.tolist(),
            [t for t in decoder_state.transpose(0, 1)])

        return seqs[0]


## ==== save and reload


def save_to(path, translater, epoch):
    torch.save({
        'init_params': translater.init_params,
        'model_state_dict': translater.state_dict(),
        'epoch': epoch
    }, path)


def reload_from(path, device):
    chk_data = torch.load(path, map_location=lambda storage, location: storage)
    translater = Translater(**chk_data['init_params'], device=device)
    translater.load_state_dict(chk_data['model_state_dict'])
    return translater.to(DEVICE), chk_data['epoch']


## ==== train

def train(translater, optimizer, scheduler, train_set, batch_size, epochs, stop, teacher_forcing_ratio=1.,
          chk_every=50, expname=''):
    training_set_generator = BatchGenerator(train_set, batch_size=batch_size)
    loss_function = nn.CrossEntropyLoss(ignore_index=Tokens.PAD)

    best_model_filename = f'{expname}best_model.chk'
    best = types.SimpleNamespace(epoch=None, loss=None, state_dict=None)
    no_improvements = 0

    for epoch in range(epochs):
        translater.train()
        start = time.perf_counter()
        epoch_loss = 0
        print(f"=== EPOCH {epoch} (lr: {scheduler.get_lr()[0]:.2g})")

        for i, batch in enumerate(training_set_generator):
            source_samples, target_samples = batch
            # reset
            optimizer.zero_grad()
            loss = translater(source_samples, target_samples, loss_function, teacher_forcing_ratio)
            # back propagate
            loss.backward()
            optimizer.step()
            epoch_loss += loss.item()

        print(f'Epoch loss [{epoch}]: {epoch_loss}. Avg loss: {epoch_loss / i}. '
              f'Elapsed: {time.perf_counter() - start:.4f}.')

        if best.loss is None or epoch_loss < best.loss:
            best.loss, best.epoch, best.state_dict = epoch_loss, epoch, translater.state_dict()
            save_to(best_model_filename, translater, epoch)
            print(f'Saved best model in {best_model_filename}')
            no_improvements = 0
        else:
            no_improvements += 1
            if no_improvements >= stop:
                print(f'No improvement for {stop} epochs. Stopping.')
                break

        scheduler.step()

        if epoch > 0 and (epoch % chk_every == 0 or epoch == epochs - 1):
            save_to(f'{expname}model-{epoch}.chk', translater, epoch)

    return best.state_dict, best.epoch
